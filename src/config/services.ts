import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
        apiKey: "AIzaSyDGjahq71fdElx3kW7CDR2zGOKA4FBCfdM",
        authDomain: "chatbout-7.firebaseapp.com",
        databaseURL: "https://chatbout-7.firebaseio.com",
        projectId: "chatbout-7",
        storageBucket: "chatbout-7.appspot.com",
        messagingSenderId: "695070527241",
        appId: "1:695070527241:web:c42fee582503caacd74dd8"
}

const Firebase = firebase.initializeApp(config);

export const Providers = {
    google: new firebase.auth.GoogleAuthProvider()
}

export const auth = firebase.auth();
export default Firebase;
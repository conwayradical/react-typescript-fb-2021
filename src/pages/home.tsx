import React from "react";
import { Link } from "react-router-dom";
import AuthContainer from "../components/AuthContainer";
import { Row, Col, Card, CardBody, Container } from "reactstrap";
import IPageProps from "../interfaces/page";

const HomePage: React.FunctionComponent<IPageProps> = (props) => {
  return (
    <AuthContainer header="Content Page">
      <Card>
        <CardBody>
          <p>Content on this page is protected</p>
          <p>
            Change your password <Link to="/change">here</Link>.
          </p>
          <p>
            Click <Link to="/logout">here</Link> to logout.
          </p>
        </CardBody>
      </Card>
    </AuthContainer>
  );
};

export default HomePage;
